$(document).ready(function () {
  localStorage.clear();
  //localStorage.removeItem("name of localStorage variable you want to remove");
});



var appState = [];
var purvendorid = '';
var purcompany = "";
var itemdetails = [];
var listcatg = [];
var listprods = [];
var itempusharry = [];
var tempdivarry = [];

var igstcal = '';

document.addEventListener("deviceready", init, false);
var appdata = new Object();


function init() {
  $(".total-amount").addClass("hide");
  $(".content").addClass("adding_topspace");
  $(".content").addClass("login_page");
  $(".signup").addClass("hide")

  appdata.loadingcommunity();
}
//loginform code here
appdata.loginform = function (email, password) {

  var dataobj = {
    email_id: email,
    password: password
  };
  api.post1('userlogin/', dataobj, function (data) {
    var logindetails = data;

    if (logindetails.length == 1) {
      // localStorage.setItem('company_id', logindetails.login_details[0]['company_id']);
      localStorage.setItem('email', logindetails[0]['email_id']);
      localStorage.setItem('full_name', logindetails[0]['first_name'] + " " + logindetails[0]['last_name']);
      localStorage.setItem('user_id', logindetails[0]['user_id']);
      localStorage.setItem('role', logindetails[0]['role']);
      localStorage.setItem('phoneno', logindetails[0]['phone_no']);
      var mes = 'Login Successfully...!';
      appdata.successmsg(mes); //DISPLAY MESSAGES
      $('.loading').addClass('spinner'); // HERE LOADER VISIBLE
      $('.login').addClass('hide'); //LOGIN HIDE HERE
      $(".loggedin").removeClass("hide"); // HEADER HIDE HERE
      $(".accountpop").removeClass("hide"); // HEADER SIDE HIDE HERE
      $(".content").addClass("login_page");
      $(".content").removeClass("adding_topspace");
      $(".small-nav").removeClass("hide");
      $('.loading').removeClass('spinner'); //HERE LOADER HIDE  
      $(".signup").addClass("hide");
      $(".total-amount").addClass("hide");
      $("#backicon_prods").addClass("hide"); // back icon for products page
      $("#carts_prods").addClass("hide"); // cart icon for products page
      $(".cart-count").addClass("hide"); // cart number for products page
      $(".user_profile").addClass("hide");
      $(".user_products").addClass("hide");
      $(".orderpage_user").addClass("hide");
      $(".issuetracker_user").addClass("hide");
      $(".user_dashboard").removeClass("hide");
      $(".faq_user").addClass("hide");
      appdata.userordercount();
      appdata.usertotalprodcount();
      appdata.userprodopen();
      appdata.userprodprogress();

      var devid =device.version;
      console.log(device)
      console.log(devid)
    }
    else {
      var mes = 'Invalid User ID or Password';
      appdata.errormsg(mes); //DISPLAY MESSAGES
      // $('.loading').removeClass('spinner'); //HERE LOADER HIDE
      $('.loading').addClass('spinner'); // HERE LOADER VISIBLE
      // $(".loggedin").removeClass("hide"); // HEADER HIDE HERE
      // $(".vendor-details").addClass("hide"); //VENDOR DETAILS VISIBLE HERE
      // $(".login").addClass("hide"); // LOGIN PAGE HIDE HERE
      // $(".content").addClass("login_page");
      $('.loading').removeClass('spinner');
    }
  });
};
//end

// date fromat convertion for service
function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}

// list of communities for user to register

appdata.loadingcommunity = function () {
  $("#community").empty();
  api.post1('getallcommunity/', '', function (data) {
    community_data = data;
    $("#community").append('<option value="0">Select Community</option>');
    $.each(community_data, function (index, values) {
      $('#community').append('<option value="' + values.community_id + '">' + values.community_name + '</option>');
    })
  })
}


//GLOBAL ALERT MESSAGES START

//success messages
appdata.successmsg = function (msg) {
  $('#msg').empty();
  $('.alertMsg').removeClass('hide');
  $('.alertMsg').addClass('sucessMsg');
  $('#msg').append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 1000);
};

//warning messages
appdata.warningmsg = function (msg) {
  $('#msg').empty();
  $('.alertMsg').removeClass('hide');
  $('.alertMsg').addClass('warningMsg');
  $('#msg').append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 500);
};

//warning messages
appdata.warningmsg_pwdchng = function (msg) {
  $('#msg').empty();
  $('.alertMsg').removeClass('hide');
  $('.alertMsg').addClass('warningMsg');
  $('#msg').append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 2000);
};

//error messages
appdata.errormsg = function (msg) {
  $('#msg').empty();
  $('.alertMsg').removeClass('hide');
  $('.alertMsg').addClass('dangerMsg');
  $('#msg').append(msg);
  $("#success-alert").show();
  setTimeout(function () {
    $("#success-alert").hide();
  }, 500);
};


//GLOBAL ALERT MESSAGES END

// SIGNUP USER 
appdata.signupuser = function () {

  var yourname = $("#yourname").val();
  var phoneno = $("#phoneno").val();
  var emailid = $("#emailsignup").val();
  var password = $("#password_signup").val();
  var communityid = $("#community").val();
  var blockname = $("#blockname").val();
  var flatno = $("#flatno").val();

  if (yourname == "" || yourname == null) {
    var messg = "Please Enter Your Name";
    appdata.warningmsg(messg);
    return;
  }

  if (phoneno == "" || phoneno == null) {
    var messg = "Please Enter Phone No";
    appdata.warningmsg(messg);
    return;
  }

  if (emailid == "" || emailid == null) {
    var messg = "Please Enter Email";
    appdata.warningmsg(messg);
    return;
  }

  if (password == "" || password == null) {
    var messg = "Please Enter Password";
    appdata.warningmsg(messg);
    return;
  }

  if (communityid == "" || communityid == null) {
    var messg = "Please Select Community";
    appdata.warningmsg(messg);
    return;
  }

  if (communityid == "" || communityid == null) {
    var messg = "Enter Block Name";
    appdata.warningmsg(messg);
    return;
  }

  if (communityid == "" || communityid == null) {
    var messg = "Enter Flat-No";
    appdata.warningmsg(messg);
    return;
  }


  var signupObj = {
    first_name: yourname,
    last_name: '',
    phone_no: phoneno,
    email_id: emailid,
    password: password,
    re_password: '',
    user_id: '',
    created_by: '1',
    updated_by: '1',
    community_id: parseInt(communityid),
    block_name: blockname,
    flat_no: flatno,
  }

  api.post1('insertuser/', signupObj, function (data) {
    signuserdata = data;
    if(data = "Inserted"){
      var mesg = "User Created";
      appdata.successmsg(mesg);
      signupObj = [];
      $(".login").removeClass("hide");
      $(".signup").addClass("hide");
      $(".orderpage_user").addClass("hide");
      $(".loggedin").addClass("hide");
      $(".accountpop").addClass("hide");
    }
    else{
      var mesg = "User Already Exists";
      appdata.successmsg(mesg);
    }
  })
}

// ORDERS LIST BY USER ID
appdata.userorderdata = function () {

  var listObj = {
    user_id: localStorage.getItem("user_id")
  }

  var txt = "";
  $("#attach-orderslist").empty();
  $("#attach-orderslist").append(txt);
  api.post1('getordertrackingdatabyuserid/', listObj, function (data) {
    var orderlist = data;
    // console.log(orderlist);
    if (orderlist.length > 0) {
      for (var i = 0; i < orderlist.length; i++) {

        var ord_stat = "'" + orderlist[i].order_status + "'"

        txt += '<tr>';
        txt += '<th><b class="ui-table-cell-label">Order ID</b>' + orderlist[i].order_id + '</th>';
        txt += '<td><b class="ui-table-cell-label">Date</b>' + orderlist[i].order_date + '</td>';
        txt += '<td><b class="ui-table-cell-label">Total Products</b> <p style="display: inline-block;"></p>' + orderlist[i].total_products + '</td>';
        txt += '<td><b class="ui-table-cell-label">Total Amount</b>' + orderlist[i].amount + '</td>';
        txt += '<td><b class="ui-table-cell-label">Transaction Status</b>' + orderlist[i].transaction_status + '</td>';
        txt += '<td><b class="ui-table-cell-label">Order Status</b>' + orderlist[i].order_status + '</td>';
        txt += '<td><div class="order-cancel"><b class="ui-table-cell-label"></b><i class="fa fa-list-alt fa-lg newitag" onclick = "getorderdetails(' + orderlist[i].order_id + ')"></i> <img class="img-size-cancel"          id="cancle_img' + orderlist[i].order_id + '" src="http://127.0.0.1:9000/static/products_images/order-cancel.png" onclick ="canceluserorder(' + orderlist[i].order_id + ',' + ord_stat + ')" ></td></div>';
        txt += '</tr>';
        txt += '<br class="hr"></br>';
      }
      $("#attach-orderslist").append(txt);
    }

    $.each(orderlist, function (index, values) {
      if(values.products_detail_status != 'OPEN'){
        $("#cancle_img"+values.order_id).addClass("hide");
      }
    });

    $('.loading').removeClass('spinner');
  })
}

// cancel user order id
function canceluserorder(ord_id, order_stat) {

  $('.loading').addClass('spinner');
  var cancelobj = {
    order_id: ord_id
  }
  api.post1('getuserordercancellation/', cancelobj, function (data) {

    if (data = 'Cancelled') {
      var msg = "Order Cancelled";
      $('.loading').removeClass('spinner');
      appdata.successmsg(msg);
      appdata.userorderdata();
    }
  })
}

// onclick of orderid to get detail orderlist
function getorderdetails(ord_id) {
  $('.loading').addClass('spinner');
  $(".orderdetailslist").removeClass("hide");
  $("#attach-detaillist").empty();
  var dt = "";
  $("#attach-detaillist").append(dt);

  var ord_obj = {
    order_id: ord_id
  }
  api.post1('getproductsordertrackingbyorderid/', ord_obj, function (data) {
    var order_details = data;
    if (order_details.length > 0) {
      for (var i = 0; i < order_details.length; i++) {
        dt += '<tr>';
        // dt += '<th><b class="ui-table-cell-label">Order ID</b>' + order_details[i].order_id + '</th>';
        dt += '<td><b class="ui-table-cell-label">Product Name</b><p class="prod-pblock addcolorp">' + order_details[i].product_name + '</p></td>';
        dt += '<td><b class="ui-table-cell-label">Unit Price</b>' + order_details[i].price + '</td>';
        dt += '<td><b class="ui-table-cell-label">Quantity</b>' + order_details[i].quantity + '</td>';
        dt += '<td><b class="ui-table-cell-label">Total Amount</b>' + order_details[i].total + '</td>';
        dt += '<td><b class="ui-table-cell-label">Expected Date</b>' + order_details[i].delivery_date + '</td>';
        dt += '<td><b class="ui-table-cell-label">Expected Time</b>' + order_details[i].delivery_time + '</td>';
        dt += '<td><b class="ui-table-cell-label">Order Status</b>' + order_details[i].order_detail_status + '</td>';
        dt += '</tr>';
        dt += '<br class="hr"></br>';
      }
      $("#attach-detaillist").append(dt);
    }
    $('.loading').removeClass('spinner');
    $(".orderpage_user").removeClass("hide");
    $(".orderlist").addClass("hide");
  })

}

// issue tracking submit functionality
appdata.issuesubmitpage = function () {

  if ($("#order_id").val() == "" || $("#order_id").val() == null) {
    var msg = "Enter Order ID";
    appdata.warningmsg(msg);
    return;
  }
  if ($("#prod_name").val() == "" || $("#prod_name").val() == null) {
    var msg = "Enter Product Name";
    appdata.warningmsg(msg);
    return;
  }
  if ($("#subj_area").val() == "" || $("#subj_area").val() == null) {
    var msg = "Enter Subject";
    appdata.warningmsg(msg);
    return;
  }
  if ($("#desc_area").val() == "" || $("#desc_area").val() == null) {
    var msg = "Enter Description";
    appdata.warningmsg(msg);
    return;
  }

  var issueobj = {
    usrOrdId: $("#order_id").val(),
    usrProdName: $("#prod_name").val(),
    usrSubject: $("#subj_area").val(),
    usrDescp: $("#desc_area").val(),
    role: localStorage.getItem("role"),
    user_id: localStorage.getItem("user_id")
  }

  api.post1('insertusrissue/', issueobj, function (data) {
    var issue_user = data;
    $('.loading').removeClass('spinner');
    if (issue_user == 'Inserted') {
      var msg = "Issue Inserted";
      appdata.successmsg(msg);
    }

    $("#order_id").val('');
    $("#prod_name").val('');
    $("#subj_area").val('');
    $("#desc_area").val('');

  })


}

// USER PROFILE DATA
appdata.userprofiledata = function () {

  // var userrole = this.storage.get('role');
  var usrobj = {
    user_id: localStorage.getItem("user_id"),
  }
  $('.loading').addClass('spinner');
  api.post1('getuserbyid/', usrobj, function (data) {
    $('.loading').removeClass('spinner');
    var userdata = data;
    // console.log(userdata);
    localStorage.setItem("community_id", userdata[0].community_id);
    $("#fistname").val(userdata[0].first_name);
    $("#lastname").val(userdata[0].last_name);
    $("#phno").val(userdata[0].phone_no);
    $("#user_email").val(userdata[0].email_id);
    $("#community_name").val(userdata[0].community_name);
    $("#block_name").val(userdata[0].block_name);
    $("#flatno").val(userdata[0].flat_no);

  })


}

// USER PROFILE UPDATE
appdata.userprofileupdate = function () {

  var updateuserobj = {
    first_name: $("#fistname").val(),
    last_name: $("#lastname").val(),
    phone_no: $("#phno").val(),
    email_id: $("#user_email").val(),
    community_id: localStorage.getItem("community_id"),
    block_name: $("#block_name").val(),
    flat_no: $("#flatno").val(),
    updated_by: localStorage.getItem("user_id"),
    user_id: localStorage.getItem("user_id")
  }

  api.post1('updateuserdetails/', updateuserobj, function (data) {
    updatestatus = data;
    $('.loading').removeClass('spinner');
    if (updatestatus == 'Inserted') {
      var msg = "Updated Successfully";
      appdata.successmsg(msg);
      return;
    }
  })

}

// DASHBOARD PAGE START
// total order count
appdata.userordercount = function () {
  var userobj = {
    user_id: localStorage.getItem('user_id')
  }
  api.post1('getuserorderscount/', userobj, function (data) {
    var ordercnt = data[0].total_orders;
    $("#user_ordercnt").text(ordercnt);
  })
}

// total products count
appdata.usertotalprodcount = function () {
  var userobj = {
    user_id: localStorage.getItem('user_id')
  }
  api.post1('getusertotalproductscount/', userobj, function (data) {
    var prodcount = data[0].inprogress_orders;
    $("#user_tot_prodcount").text(prodcount);
  })
}

// user products open
appdata.userprodopen = function () {
  var userobj = {
    user_id: localStorage.getItem('user_id')
  }
  api.post1('getuserorderscountforopenstatus/', userobj, function (data) {
    var prodstatus = data[0].open_orders;
    $("#user_prod_open").text(prodstatus);
  })
}

// user products inprogress
appdata.userprodprogress = function () {
  var userobj = {
    user_id: localStorage.getItem('user_id')
  }
  api.post1('getuserorderscountforinprogressstatus/', userobj, function (data) {
    var prodprogressstatus = data[0].inprogress_orders;
    $("#user_prod_inprogress").text(prodprogressstatus);
    $('.loading').removeClass('spinner');
  })
}

// DASHBOARD PAGE END

// PRODUCTS PAGE

//GET ALL PRODUCTS IN PRODUCTS PAGE

appdata.getallproducts = function () {
  api.post1('getallproducts/', '', function (data) {
    // console.log("list of products");
    // console.log(data);
    listprods = data;
    appdata.getcategorylist();
  })
}


// GET ALL CATEGORIES  OF PRODUCTS PAGE
appdata.getcategorylist = function () {
  $("#backicon_prods").addClass("hide");
  $("#prods").empty();
  api.post1('getActiveCategory/', '', function (data) {
    // console.log("list of categories")
    // console.log(data);
    listcatg = data;
    $("#prods").append('<option value="Select_Category">Select Category</option>');

    $.each(listcatg, function (index, values) {
      $('#prods').append('<option value="' + values.category_id + '">' + values.category_name + '</option>');
    });
    $("#items_list_attach").empty();

    appdata.getproductslist();

  })
}






// GET ALL PRODUCTS WITHOUT CATEGORY
appdata.getproductslist = function () {
  $('.loading').removeClass('spinner');

  var txt = "";
  itempusharry = [];


  for (var i = 0; i < listcatg.length; i++) {
    $("#items_list_attach").append('<div class="' + listcatg[i].category_id + '" id ="catg-div-' + listcatg[i].category_id + '"></div>');
    txt = ""
    for (var j = 0; j < listprods.length; j++) {
      if (listprods[j].category_id == listcatg[i].category_id) {

        //storing ids in to variables
        var itemqtylbl = "'" + 'qty-val' + listcatg[i].category_id + j + "'"; //input val for product details page 
        var prod_id = "'" + j + "'"; //product index in array
        var img_add = appSettings.staticurl;// image endpoint
        var divcartid = "'" + 'main_prodscart' + j + "'";
        var cat_nm = "'#qty-val" + listcatg[i].category_id + j + "'";
        var flag = 2;
        // var _nm = "'#qty-val" + listcatg[i].category_name + j + "'";

        txt += '<div class="mob-products"  id="prod-div-' + j + '">';
        txt += '<div class="row">';
        txt += '<div class="col-xs-5 img-responsive product-img" onclick="getdetailsproduct(' + listprods[j].product_id + ', ' + itemqtylbl + ',' + prod_id + ',' + divcartid + ')">';
        txt += '<img src="' + img_add + listprods[j].product_image + '">';
        txt += '</div>';
        txt += '<div class="col-xs-7 product_name">';
        txt += '<h6>' + listprods[j].product_name + ' <span>(' + listprods[j].weight + ')</span></h6>';
        txt += '<div class="prdt-details">';
        txt += '<p>Category : <span>' + listprods[j].category_name + '</span></p>';
        // txt += '<p>Brand : <span>'+itempusharry[j].brand_name+'</span></p>';
        txt += '<p>Price : <span>' + listprods[j].price + '</span></p>';
        // txt += '<p>Measurement : <span>'+itempusharry[j].weight+'</span></p>';
        txt += '</div>';
        txt += '</div>';
        txt += '</div>';
        txt += '<div class="row mgt-5">';
        txt += '<div class="col-xs-6">';
        txt += '<div class="product-icon" id="count-minus' + j + '" onclick="prodcountminus(' + itemqtylbl + ',' + j + ',' + cat_nm + ',' + flag + ')" ><i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i></div>';
        txt += '<div class="product-input"><input  type="text" id="qty-val' + listcatg[i].category_id + j + '" class="form-control prod_img_size" disabled maxlength="3" id="prodQty' + j + '" value=' + listprods[j].qty + ' /></div>';
        txt += '<div class="product-icon" id="count-plus' + j + '" onclick="prodcountplus(' + itemqtylbl + ',' + j + ',' + cat_nm + ',' + flag + ')"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></div>';
        txt += '</div>';
        txt += '<div class="col-xs-6">';
        txt += '<div style="color: coral; text-align: right;padding-right: 10px;">';
        // txt += '<i class="fa fa-shopping-cart fa-lg" aria-hidden="true"></i>';
        txt += '<div class="add-cart" id="main_prodscart' + j + '" ';
        txt += 'onclick="addprodstocart(' + listprods[j].product_id + ',' + divcartid + ',' + itemqtylbl + ',' + flag + ')" >';
        txt += '<p>Add to Cart</p>';
        txt += '</div>';
        txt += '</div>';
        txt += '</div>';
        txt += '</div>';
        txt += '</div>';
        txt += '<div id="get_prod_div" class="hide">' + listprods[j].product_id + '</div>';

      }
    }
    $("#catg-div-" + listcatg[i].category_id).append(txt);

  }

}

var myarr = []

// div show/hide upon category selected list
appdata.getproductscategorylist = function (selval) {
  // myarr = [];
  $('.items_list > div').map(function () {
    myarr.push(this.className);
  });

  for (var i = 0; i < myarr.length; i++) {

    if (selval == "Select_Category") {
      $("." + myarr[i]).removeClass("hide");
    }
    else if (myarr[i] == selval) {
      $("." + myarr[i]).removeClass("hide");
    }
    else {
      $("." + myarr[i]).addClass("hide");
    }

  }

}




// get individual product details
function getdetailsproduct(prodid, qtyvalue, idindex, cartdivid) {
  var qtyval = $("#" + qtyvalue).val();
  // var qtyid = "'" + qtyvalue + "'";

  var prodObj = {
    product_id: prodid
  }
  $('.loading').addClass('spinner');
  var adtxt = "";
  $("#items_detail_attach").empty();
  $("#items_detail_attach").append(adtxt);
  // var img_add = api.img_static;
  var img_add = appSettings.staticurl;
  api.post1('getproductbyid/', prodObj, function (data) {
    var proddet = data;
    // console.log(proddet);
    // console.log(itempusharry[idindex].product_id)

    var detqtylbl = "'" + 'prodQty_detail' + "'";
    var inner_ip = "'#" + 'prodQty_detail' + "'";
    // var divcartid = "'" + 'main_prodscart' + j + "'";
    var flag_inner = 1;

    adtxt += '<div class="mob-products-details">';
    adtxt += '<div>';
    adtxt += '<div class="product-details">';
    adtxt += '<div class="item-brand">';
    adtxt += '<p>Godrej</p>';
    adtxt += '</div>';
    adtxt += '<p>' + proddet[0].category_name + ' - ' + proddet[0].product_name + '<span> ( ' + proddet[0].measurement + '' + proddet[0].unit_type + ' )</span></p>';
    adtxt += '<h6>Rs <span>' + proddet[0].price + '</span></h6>';
    adtxt += '</div>';
    adtxt += '<div class="img-responsive product-img ">';
    adtxt += '<img src="' + img_add + proddet[0].product_image + '">';
    adtxt += '</div>';
    adtxt += '</div>';
    adtxt += '<div class="prod-row-cart">';
    adtxt += '<div class="row">';
    adtxt += '<div class="col-xs-6">';
    adtxt += '<div class="qty-align">';
    adtxt += '<p>Qty :</p>';
    adtxt += '</div>';
    adtxt += '<div class="product-icon" id="count-detpage-minus" onclick="prodcountminus(' + detqtylbl + ',' + 0 + ',' + inner_ip + ',' + flag_inner + ')"><i class="fa fa-minus-circle fa-lg" aria-hidden="true"></i>';
    adtxt += '</div>';
    adtxt += '<div class="product-input">';
    adtxt += '<input type="text" class="form-control prod_img_size" maxlength="3" disabled id="prodQty_detail" value="1" /></div>';
    adtxt += '<div class="product-icon" id="count-detpage-plus" onclick="prodcountplus(' + detqtylbl + ',' + 0 + ',' + inner_ip + ',' + flag_inner + ')">';
    adtxt += '<i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i>';
    adtxt += '</div>';
    adtxt += '</div>';
    adtxt += '<div class="col-xs-6">';
    adtxt += '<div class="add-cart-btn" id="inner_prodscart" onclick="addprodstocart(' + proddet[0].product_id + ',' + cartdivid + ',' + detqtylbl + ',' + flag_inner + ')">';
    adtxt += '<p>Add to Cart</p>';
    adtxt += '</div>';
    adtxt += '</div>';
    adtxt += '</div>';
    adtxt += '</div>';
    adtxt += '<div class="prod-info">';
    adtxt += '<h6>About</h6>';
    adtxt += '<p>' + proddet[0].product_description + '</p>';
    adtxt += '</div>';
    adtxt += '</div>';
    adtxt += '<div id="get_prod_inner" class="hide">' + proddet[0].product_id + '</div>';

    $("#items_detail_attach").append(adtxt);
    $("#prodQty_detail").val(qtyval);

    if ($("#" + cartdivid).css('display') == 'none') {
      $('#inner_prodscart').addClass("hide");
    }

    $("#items_list_attach").addClass("hide");
    $("#items_detail_attach").removeClass("hide");
    $("#prods_dropdown").addClass("hide");
    $("#prods_head").addClass("hide");
    $(".small-nav").addClass("hide");
    $("#backicon_prods").removeClass("hide");
    $(".new_date").addClass("hide");
    $('.loading').removeClass('spinner');
  })

}

var hours_arry = ['8 AM', '9 AM', '10 AM', '11 AM', '12 PM', '1 PM', '2 PM', '3 PM', '4 PM', '5 PM', '6 PM', '7 PM', '8 PM'];

var count_val = 0;
var cartselected = [];
var temparrylst = [];
//add to cart functionality common method
function addprodstocart(prod_id, cart_lbl, qty_lbl, act_flag) {
  $(".new_date").addClass("hide");



  // console.log("product id:" + prod_id + ',' + "cart div:" + cart_lbl + ',' + "qty_input_lbl :" + qty_lbl);

  if (act_flag == 2) {

    var cartbtnid = "#" + cart_lbl;
    $(cartbtnid).addClass("hide"); //hide cart main div 
    count_val++;
    $("#cart-cnt-val").html(count_val);

    for (var i = 0; i < listprods.length; i++) {

      if (prod_id == listprods[i].product_id) {
        var temp2 = {
          brand_name: listprods[i].brand_name,
          category_id: listprods[i].category_id,
          category_name: listprods[i].category_name,
          is_active: listprods[i].is_active,
          price: listprods[i].price,
          prod_total: listprods[i].price,
          product_id: listprods[i].product_id,
          product_image: listprods[i].product_image,
          product_name: listprods[i].product_name,
          qty: listprods[i].qty,
          weight: listprods[i].weight,
          button_div_id: cart_lbl,
          input_label: qty_lbl,
          delivery_date: $("#globaldate").val(),
          delivery_time: hours_arry[2]
        }
        temparrylst.push(temp2);
      }
    }
    // console.log(temparrylst);
  }

  else {

    var cartbtnid = "#" + cart_lbl["id"];
    $(cartbtnid).addClass("hide"); //hide cart main div 
    count_val++;
    $("#cart-cnt-val").html(count_val);
    $("#inner_prodscart").addClass("hide");

    for (var i = 0; i < listprods.length; i++) {
      if (prod_id == listprods[i].product_id) {
        var temp2 = {
          brand_name: listprods[i].brand_name,
          category_id: listprods[i].category_id,
          category_name: listprods[i].category_name,
          is_active: listprods[i].is_active,
          price: listprods[i].price,
          prod_total: listprods[i].price,
          product_id: listprods[i].product_id,
          product_image: listprods[i].product_image,
          product_name: listprods[i].product_name,
          qty: listprods[i].qty,
          weight: listprods[i].weight,
          button_div_id: cart_lbl["id"],
          input_label: qty_lbl,
          delivery_date: $("#globaldate").val(),
          delivery_time: hours_arry[2]
        }
        temparrylst.push(temp2);
      }
    }
    // console.log(temparrylst);
  }

}

var calc_tot = 0;

appdata.sel_product_cart = function () {
  var crt = "";
  var flg = 3;
  calc_tot = 0;
  $('#globaltime').empty();
  $(".new_date").removeClass("hide");
  $(".total-amount").removeClass("hide");
  $("#products_checout").empty();
  var img_add = appSettings.staticurl;// image endpoint

  for (var k = 0; k < temparrylst.length; k++) {
    for (var j = 0; j < listprods.length; j++) {
      if (listprods[j].product_id == temparrylst[k].product_id) {
        temparrylst[k].qty = listprods[j].qty

      }
    }
  }

  // binding data to cart  after updating quantity value
  $("#tot_price").text(calc_tot.toFixed(2));
  for (var i = 0; i < temparrylst.length; i++) {

    cartdivid = "'" + 'chkdivprods' + i + "'";
    var cartqtylbl = "'" + 'prod_qty_val' + temparrylst[i].category_id + i + "'";
    var hideprodlbl = "'" + 'get_cart_inner' + temparrylst[i].product_id + "'";

    crt += '<div class="chk-products" id="chkdivprods' + i + '">';
    crt += '<div class="del-icon-pos" id="del_prod_cart" onclick="deletecartitem(' + cartdivid + ',' + temparrylst[i].product_id + ')">';
    crt += '<i class="fa fa-times-circle" aria-hidden="true"></i>';
    crt += '</div>';
    crt += '<div class="row">';
    crt += '<div class="col-xs-5 img-responsive product-img ">';
    crt += '<img src="' + img_add + temparrylst[i].product_image + '">';
    crt += '</div>';
    crt += '<div class="col-xs-7 product_name">';
    crt += '<h6>' + temparrylst[i].product_name + ' <span>(' + temparrylst[i].weight + ')</span></h6>';
    crt += '<div class="prdt-details">';
    crt += '<p>Unit Price : <span>' + temparrylst[i].price + '</span></p>';
    crt += '<div>';
    crt += '<div class="product-icon" onclick="prodcountminus(' + cartqtylbl + ',' + i + ',' + hideprodlbl + ',' + flg + ')"><i class="fa fa-minus-circle fa-lg"  aria-hidden="true"></i>';
    crt += '</div>';
    crt += '<div class="product-input"><input type="text" class="form-control prod_img_size" disabled maxlength="4" id="prod_qty_val' + temparrylst[i].category_id + i + '" value="' + temparrylst[i].qty + '" />';
    crt += '</div>';
    crt += '<div class="product-icon" onclick="prodcountplus(' + cartqtylbl + ',' + i + ',' + hideprodlbl + ',' + flg + ')"><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i>';
    crt += '</div>';
    crt += '<div class="clear"></div>';
    crt += '</div>';
    crt += '</div>';
    crt += '</div>';
    crt += '</div>';
    // date and time start
    crt += '<div class="row">';
    crt += '<div class="form-group col-md-6">';
    crt += '<label for="indivdate">Delivery Date</label>';
    crt += '<input type="text" onchange="individualprddate(' + temparrylst[i].product_id + ',' + i + ')" id="indivdate' + temparrylst[i].product_id + '">';
    crt += '</div>';
    crt += '<div class="form-group col-md-6">';
    crt += '<label>Delivery Time</label>';
    crt += '<select id="indi_deli_time' + temparrylst[i].product_id + '" onchange="individualprdtime(' + temparrylst[i].product_id + ',' + i + ')" name="delivery_time">';
    crt += '</select>';
    crt += '</div>';
    crt += '</div>';
    // date and time end

    crt += '</div>';
    crt += '<div id="get_cart_inner' + temparrylst[i].product_id + '" class="hide">' + temparrylst[i].product_id + '</div>';

    calc_tot += parseInt(temparrylst[i].qty) * parseFloat(temparrylst[i].price)  // calculation of total

  }

  $("#products_checout").append(crt);

  // setting global date in cart for every product
  $.each(hours_arry, function (index, values) {
    $('#globaltime').append('<option value="' + values + '">' + values + '</option>');
  });

  // setting individual date in cart
  $.each(temparrylst, function (index, values) {
    $("#indivdate" + values.product_id).datepicker().datepicker('setDate', new Date());
  });

  // setting individual dropdown in cart
  for (var i = 0; i < temparrylst.length; i++) {
    $.each(hours_arry, function (index, values) {
      $('#indi_deli_time' + temparrylst[i].product_id).append('<option value="' + values + '">' + values + '</option>')
    });
    $('#indi_deli_time' + temparrylst[i].product_id).val(hours_arry[2])
  }

  $('#globaltime').val(hours_arry[2]); // default global time

  $("#tot_price").text(calc_tot.toFixed(2));

}

// delete an item from cart 
function deletecartitem(divid, prod_id) {
  var div_lbl = "";
  var ival = "1";

  for (var j = 0; j < listprods.length; j++) {
    if (prod_id == listprods[j].product_id) {
      listprods[j].qty = ival;
      // console.log(listprods);
    }
  }

  for (var i = 0; i < temparrylst.length; i++) {
    var ival = "1";
    if (prod_id == temparrylst[i].product_id) {
      div_lbl = temparrylst[i].button_div_id;
      $("#" + temparrylst[i].input_label).val(ival);
      $("#" + div_lbl).removeClass("hide");
      calc_tot -= parseInt(temparrylst[i].qty) * parseFloat(temparrylst[i].price)  // calculation of total
      temparrylst.splice(i, 1);
      count_val--;
      $("#cart-cnt-val").html(count_val);
    }
  }
  $("#tot_price").text(calc_tot.toFixed(2));
  $("#" + divid).remove(); // removing div in cartpage

}

// making payment method
appdata.productspayment = function () {

  for (var i = 0; i < temparrylst.length; i++) {
    for (var j = 0; j < listprods.length; j++) {
      if (temparrylst[i].product_id == listprods[j].product_id) {
        temparrylst[i].qty = listprods[j].qty
        temparrylst[i].delivery_date = formatDate(temparrylst[i].delivery_date)
        temparrylst[i].prod_total = temparrylst[i].price * temparrylst[i].qty
      }
    }
  }

  var payObj = {
    user_id: localStorage.getItem("user_id"),
    transaction_status: "SUCCESS",
    total_amount: $("#tot_price").text(),
    total_products: temparrylst.length,
    order_detail: temparrylst,
    payment_id : razorpayId
  }
  // console.log(payObj);

  if (parseInt($("#tot_price").text()) <= 0) {
    var msg = "Please Add Products to Cart";
    appdata.warningmsg(msg);
    return;
  }

  api.post1('insertuserorder/', payObj, function (data) {
    var insertresult = data;

    itempusharry = [];
    temparrylst = [];
    listprods = [];
    count_val = '0';

    $("#cart-cnt-val").text("0");
    $("#products_checout").empty();
    $("#items_detail_attach").empty();
    $("#items_list_attach").empty();
    $("#prods_head").removeClass("hide");
    $("#prods_dropdown").removeClass("hide");
    $("#backicon_prods").addClass("hide");
    $(".small-nav").removeClass("hide");
    $(".total-amount").addClass("hide");
    $(".new_date").addClass("hide");
    // appdata.getproductscategorylist($("#prods").val("0"));
    appdata.getallproducts();
    $("#items_list_attach").removeClass("hide");
    $('.loading').addClass('spinner');
    if (insertresult == "Inserted") {
      var msg = "Order created successfully"
      appdata.successmsg(msg);
      $("#cart-cnt-val").text("0");
    }
  })

}

// applying all products single delivery time
appdata.productstimechange = function () {

  var glb_time = $("#globaltime option:selected").text();

  $.each(temparrylst, function (index, values) {
    $('#indi_deli_time' + values.product_id).val(glb_time);
    temparrylst[index].delivery_time = glb_time;
  });
}

// applying all products single delivery date
function productsdatechange(){

  var glb_date = formatDate($('#globaldate').val());

  $.each(temparrylst, function (index, values) {
    $('#indivdate' + values.product_id).val($("#globaldate").val());
    temparrylst[index].delivery_date = glb_date;
  });
  // console.log(temparrylst)
}

function individualprddate(prodid, indx) {
  var indi_date = formatDate($('#indivdate' + prodid).val());
  temparrylst[indx].delivery_date = indi_date;
  // console.log(temparrylst)
}

function individualprdtime(prodid, indx) {
  var idval = '#indi_deli_time' + prodid;
  var indi_time = $(idval).val();
  temparrylst[indx].delivery_time = indi_time;
  // console.log(temparrylst)
}

