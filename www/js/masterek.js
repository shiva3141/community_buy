var body = $('body');
var data123 = {};
var totalitemqty = "";
var selectedVendor = '';
var selectedCustomer = '';
var overlay = new Object();
var razorpayId = '';




overlay.show = function (addClass) {
	$('.overlay').addClass('on').show();
	if (addClass != undefined) {
		$('.overlay').addClass(addClass);
	}
};
overlay.hide = function () {
	$('.overlay').removeClass('on').hide();
	$('.overlay-hide').hide('fast');
	$('.overlay-remove').remove();
};
body.on('click', '.accountpop', function (e) {
	e.stopPropagation();
});
body.on('click', '.act-my-account', function (e) {
	e.stopPropagation();
	$('.accountpop').css({
		left: 0
	});
});
body.on('click', '.side-barmenu', function (e) {
	e.stopPropagation();
	$('.accountpop').css({
		left: '-275px'
	});
});
$(document).on("click", function () {
	$(".accountpop").css({
		left: '-275px'
	});
});


var $payBundle = $('.js-pay-bundle');

$payBundle.on('click', function () {

	if (parseInt($("#tot_price").text()) <= 0) {
		var msg = "Please Add Products to Cart";
		appdata.warningmsg(msg);
		return;
	}

	var itemId = $(this).data('itemid');
	// var	amount = $(this).data('amount');
	var amount = parseFloat($('#tot_price').text());
	var processorid = $(this).data('processorid');
	var qty = $(this).data('qty');

	callRazorPayScript(itemId, amount, processorid, qty);
});

var callRazorPayScript = function (itemId, amount, processorId, qty) {
	// alert();
	var merchangeName = localStorage.getItem('full_name');
	var img = "https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg";
	var name = localStorage.getItem('full_name');
	var description = "Buying Fruits";
	var amount = amount;
	var qty = qty;

	loadExternalScript('https://checkout.razorpay.com/v1/checkout.js').then(function () {
		var options = {
			key: 'rzp_test_7kMLm6TgFDyDqi', //test key
			// key: 'rzp_live_ILgsfZCZoFIKMb', //live key
			protocol: 'https',
			// redirect:true,
			hostname: 'api.razorpay.com',
			amount: amount * 100,
			name: merchangeName,
			description: description,
			image: img,
			prefill: {
				name: name,
			},
			theme: {
				color: '#15b8f3'
			},
			// handler: function (transaction, response){
			//   console.log('Tshirt\\ntransaction id: ' + transaction.razorpay_payment_id);
			//   console.log(response.razorpay_payment_id)
			// }
			handler: function (response) {
				// console.log(JSON.stringify(response));
				if (response.razorpay_payment_id == 'undefined' || response.razorpay_payment_id < 1) {
					alert('Payment failed');
				} else {
					razorpayId = response.razorpay_payment_id;
					// appdata.paymentinfo();
					appdata.productspayment();
				}
			}
		};

		window.rzpay = new Razorpay(options);

		// console.log('Item Id: ', amount);
		// console.log('Amount: ', amount);
		// console.log('Quantity: ', qty);
		// console.log('Processor Id: ', processorId);

		rzpay.open();

	});
}


var loadExternalScript = function (path) {
	var result = $.Deferred(),
		script = document.createElement("script");

	script.async = "async";
	script.type = "text/javascript";
	script.src = path;
	script.onload = script.onreadystatechange = function (_, isAbort) {
		if (!script.readyState || /loaded|complete/.test(script.readyState)) {
			if (isAbort)
				result.reject();
			else
				result.resolve();
		}
	};

	script.onerror = function () {
		result.reject();
	};

	$("head")[0].appendChild(script);

	return result.promise();
};




//DROPDOWN Menu
body.on('click', '.side-menubar', function () {
	const t = $(this);
	t.toggleClass('open');
	t.next().toggle('slow');
	if ($(this).attr('id') == 'menu1') {
		if ($('#menu1').hasClass("open")) {
			$('#list1').css('display', 'block');
			$('#menu2').removeClass('open');
			$('#list2').css('display', 'none');
		} else {
			$('#menu1').removeClass("open");
			$('#list1').css('display', 'none');
		}
	}
	if ($(this).attr('id') == 'menu2') {
		if ($('#menu2').hasClass("open")) {
			$('#list2').css('display', 'block');
			$('#menu1').removeClass('open');
			$('#list1').css('display', 'none');
		} else {
			$('#menu2').removeClass("open");
			$('#list2').css('display', 'none');
		}
	}
});

//menu routing code here
body.on('click', '.act-menu', function () {
	var t = $(this);
	// var extra = t.data('extra');
	// if (extra == '') { 
	// 	extra = 'rescan';
	// }
	pageName = t.attr('href');
	pageName = pageName.substr(1);
	// page.route(pageName, $(this), extra);

	// DadhBoard
	if (pageName == "dashboard") {
		$(".loggedin").removeClass("hide");
		$(".small-nav").removeClass("hide");
		$(".login").addClass("hide");
		$("#backicon_prods").addClass("hide");
		$("#carts_prods").addClass("hide");
		$(".cart-count").addClass("hide");
		$(".content").removeClass("adding_topspace");
		$(".orderpage_user").addClass("hide");
		$(".signup").addClass("hide");
		$(".total-amount").addClass("hide");
		$(".orderdetailslist").addClass("hide");
		$(".issuetracker_user").addClass("hide");
		$(".user_profile").addClass("hide");
		$(".user_products").addClass("hide");
		$(".user_dashboard").removeClass("hide");
		$(".user_reports").addClass("hide");
		$('.loading').addClass('spinner');
		$(".user_changepwd").addClass("hide");
		$(".faq_user").addClass("hide");
		appdata.userordercount();
		appdata.usertotalprodcount();
		appdata.userprodopen();
		appdata.userprodprogress();


	}

	// PROFILE PAGE
	if (pageName == "profile") {
		$(".loggedin").removeClass("hide");
		$(".small-nav").removeClass("hide");
		$(".login").addClass("hide");
		$(".content").removeClass("adding_topspace");
		$("#backicon_prods").addClass("hide");
		$("#carts_prods").addClass("hide");
		$(".cart-count").addClass("hide");
		$(".orderpage_user").addClass("hide");
		$(".signup").addClass("hide");
		$(".total-amount").addClass("hide");
		$(".orderdetailslist").addClass("hide");
		$(".issuetracker_user").addClass("hide");
		$(".user_profile").removeClass("hide");
		$(".user_products").addClass("hide");
		$(".user_changepwd").addClass("hide");
		$(".user_dashboard").addClass("hide");
		$(".faq_user").addClass("hide");
		$(".user_reports").addClass("hide");

		appdata.userprofiledata();

	}

	//reports page
	if (pageName == "reports") {
		$(".loggedin").removeClass("hide");
		$(".small-nav").removeClass("hide");
		$(".login").addClass("hide");
		$(".content").removeClass("adding_topspace");
		$("#backicon_prods").addClass("hide");
		$("#carts_prods").addClass("hide");
		$(".cart-count").addClass("hide");
		$(".orderpage_user").addClass("hide");
		$(".signup").addClass("hide");
		$(".total-amount").addClass("hide");
		$(".orderdetailslist").addClass("hide");
		$(".issuetracker_user").addClass("hide");
		$(".user_profile").addClass("hide");
		$(".user_products").addClass("hide");
		$(".user_changepwd").addClass("hide");
		$(".user_dashboard").addClass("hide");
		$(".faq_user").addClass("hide");
		// $('.loading').addClass('spinner');
		$(".user_reports").removeClass("hide");


	}


	// change password
	if (pageName == "changepwd") {
		$(".loggedin").removeClass("hide");
		$(".small-nav").removeClass("hide");
		$(".login").addClass("hide");
		$("#backicon_prods").addClass("hide");
		$(".content").removeClass("adding_topspace");
		$("#carts_prods").addClass("hide");
		$(".cart-count").addClass("hide");
		$(".cart-count").addClass("hide");
		$(".total-amount").addClass("hide");
		$(".faq_user").addClass("hide");
		$(".user_dashboard").addClass("hide");
		$(".orderpage_user").addClass("hide");
		$(".signup").addClass("hide");
		$(".orderdetailslist").addClass("hide");
		$(".user_products").addClass("hide");
		// $('.loading').addClass('spinner');
		$(".issuetracker_user").addClass("hide");
		$(".user_profile").addClass("hide");
		$(".user_changepwd").removeClass("hide");
		$(".user_reports").addClass("hide");
	}

	// PRODUCTS PAGE
	if (pageName == "products") {
		$(".loggedin").removeClass("hide");
		$(".small-nav").removeClass("hide");
		$(".login").addClass("hide");
		$(".orderpage_user").addClass("hide");
		$(".content").removeClass("adding_topspace");
		$(".signup").addClass("hide");
		$(".orderdetailslist").addClass("hide");
		$(".issuetracker_user").addClass("hide");
		$(".user_profile").addClass("hide");
		$("#items_list_attach").removeClass("hide");
		$("#items_detail_attach").addClass("hide");
		$("#prods_dropdown").removeClass("hide");
		$(".total-amount").addClass("hide");
		$(".user_changepwd").addClass("hide");
		$("#prods_head").removeClass("hide");
		$(".user_dashboard").addClass("hide");
		$('.loading').addClass('spinner');
		$("#backicon_prods").removeClass("hide");
		$("#carts_prods").removeClass("hide");
		$(".faq_user").addClass("hide");
		$(".cart-count").removeClass("hide");
		$(".user_products").removeClass("hide");
		$(".user_reports").addClass("hide");
		$("#products_checout").addClass("hide");
		$("#products_checout").empty();
		$(".new_date").addClass("hide");
		$("#cart-cnt-val").text("0");
		// appdata.getcategorylist(); // get all categories
		appdata.getallproducts(); // get all list of products
		$("#items_list_attach").empty();

	}

	// ORDER BOOKING PAGE
	if (pageName == "orderbook") {
		$(".loggedin").removeClass("hide");
		$(".small-nav").removeClass("hide");
		$(".login").addClass("hide");
		$("#backicon_prods").addClass("hide");
		$(".content").removeClass("adding_topspace");
		$("#carts_prods").addClass("hide");
		$(".cart-count").addClass("hide");
		$(".cart-count").addClass("hide");
		$(".total-amount").addClass("hide");
		$(".faq_user").addClass("hide");
		$(".user_dashboard").addClass("hide");
		$(".orderpage_user").removeClass("hide");
		$(".signup").addClass("hide");
		$(".orderdetailslist").addClass("hide");
		$(".user_products").addClass("hide");
		$(".user_changepwd").addClass("hide");
		$(".user_reports").addClass("hide");
		$('.loading').addClass('spinner');
		$(".issuetracker_user").addClass("hide");
		$(".user_profile").addClass("hide");
		appdata.userorderdata();
	}

	//FAQ'S PAGE
	if (pageName == "faq") {
		$(".loggedin").removeClass("hide");
		$(".small-nav").removeClass("hide");
		$(".login").addClass("hide");
		$(".orderpage_user").addClass("hide");
		$(".signup").addClass("hide");
		$(".orderdetailslist").addClass("hide");
		$(".content").removeClass("adding_topspace");
		$(".issuetracker_user").addClass("hide");
		$(".user_profile").addClass("hide");
		$("#items_list_attach").addClass("hide");
		$("#items_detail_attach").addClass("hide");
		$("#prods_dropdown").addClass("hide");
		$(".total-amount").addClass("hide");
		$("#prods_head").removeClass("hide");
		$(".user_dashboard").addClass("hide");
		// $('.loading').addClass('spinner');
		$("#backicon_prods").addClass("hide");
		$(".user_changepwd").addClass("hide");
		$("#carts_prods").addClass("hide");
		$(".cart-count").addClass("hide");
		$(".user_products").addClass("hide");
		$("#products_checout").addClass("hide");
		$("#items_list_attach").empty();
		$(".faq_user").removeClass("hide");
		$(".user_reports").addClass("hide");

	}

	// ISSUE TRACKER PAGE
	if (pageName == "issuetracker") {
		$(".loggedin").removeClass("hide");
		$(".small-nav").removeClass("hide");
		$(".login").addClass("hide");
		$(".orderpage_user").addClass("hide");
		$(".signup").addClass("hide");
		$(".content").removeClass("adding_topspace");
		$(".total-amount").addClass("hide");
		$(".orderdetailslist").addClass("hide");
		$("#backicon_prods").addClass("hide");
		$(".user_dashboard").addClass("hide");
		$("#carts_prods").addClass("hide");
		$(".cart-count").addClass("hide");
		$(".issuetracker_user").removeClass("hide");
		$(".user_changepwd").addClass("hide");
		$(".user_profile").addClass("hide");
		$(".faq_user").addClass("hide");
		$(".user_products").addClass("hide");
		$(".user_reports").addClass("hide");
	}

	if (pageName == "logout") {
		$(".orderpage_user").addClass("hide");
		$(".login").removeClass("hide");
		$(".loggedin").addClass("hide");
		$(".user_dashboard").addClass("hide");
		$(".content").addClass("adding_topspace");
		$(".total-amount").addClass("hide");
		$(".accountpop").addClass("hide");
		$(".user_changepwd").addClass("hide");
		$(".signup").addClass("hide");
		$(".faq_user").addClass("hide");
		$(".user_reports").addClass("hide");
		localStorage.clear();
	}

})






//login code for user start
body.on('click', '.loginfom', function () {
	$('.loading').addClass('spinner');
	var email = $("#email").val();
	var password = $("#password1").val();
	if (email == "") {
		var mes = "Please enter user name";
		appdata.warningmsg(mes);
		$('.loading').removeClass('spinner');
		return;
	}
	if (password == "") {
		var mes = "Please enter password";
		appdata.warningmsg(mes);
		$('.loading').removeClass('spinner');
		return;
	}

	appdata.loginform(email, password); //login method
});


//login code for user end

//signup code for user start
body.on('click', '#signupuser', function () {

	$(".login").addClass("hide");
	$(".orderpage_user").addClass("hide");
	$(".loggedin").addClass("hide");
	$(".accountpop").addClass("hide");
	$(".signup").removeClass("hide");

	$("#yourname").val("");
	$("#phoneno").val("");
	$("#emailsignup").val("");
	$("#password_signup").val("");
	$("#community").val("0");
	$("#blockname").val("");
	$("#flatno").val("");

})

//signup code for user end

// signup cancel button
$("#signcancel").click(function () {
	$(".login").removeClass("hide");
	$(".signup").addClass("hide");
	$(".orderpage_user").addClass("hide");
	$(".loggedin").addClass("hide");
	$(".accountpop").addClass("hide");

	// clearing signup values
	$("#yourname").val('');
	$("#phoneno").val('');
	$("#emailsignup").val('');
	$("#password_signup").val('');
	$("#community").val('0');

})

// signing up for new user
$("#signsubmit").click(function () {
	appdata.signupuser();
})

// back arrow to show orders list
$("#backtolist").click(function () {

	$(".orderdetailslist").addClass("hide");
	$(".orderlist").removeClass("hide");
	$("#attach-detaillist").empty();


})

// issue tracking user submit button
$("#issue_submit").click(function () {
	$('.loading').addClass('spinner');
	appdata.issuesubmitpage();
})

// user profile update

$("#profileupdate").click(function () {
	$('.loading').addClass('spinner');
	appdata.userprofileupdate();
})

// displaying list of products by selection
$("#prods").on('change', function () {
	appdata.getproductscategorylist($("#prods option:selected").val()); // get list of products by category id
});


// decreasing product count 
function prodcountminus(id, prodindexid, cat_name, flg) {
	var counter = $('#' + id).val();
	if (counter == NaN || counter <= 1) {
		var counter = $('#' + id).val("1");
		var msg = "Minumum Product Count";
		appdata.warningmsg(msg);
		return;
	}
	counter--;
	var innerprdid = $('#get_prod_inner').text();
	// for details product view decrement
	if (flg == 2) {
		if ($('#prodQty_detail').length) {
			// var innerprdid = $('#get_prod_inner').text();
			$('#' + id).val(counter);
			listprods[prodindexid].qty = counter;
			for (var i = 0; i < listprods.length; i++) {
				if (listprods[i].product_id == innerprdid) {
					$('#qty-val' + listprods[i].category_id + i).val(counter);
					listprods[i].qty = counter;
				}
			}
			// console.log(listprods)
		}
		else {
			$('#' + id).val(counter);
			// console.log("decrease");
			listprods[prodindexid].qty = counter;
			// console.log(listprods);
		}
	} else if (flg == 3) {
		$('#' + id).val(counter);
		var calc_tot1 = parseFloat($("#tot_price").text());
		// console.log("decrease");
		for (var i = 0; i < listprods.length; i++) {
			var innerprdid = $('#' + cat_name).html();

			if (listprods[i].product_id == innerprdid) {
				listprods[i].qty = counter;
				$('#qty-val' + listprods[i].category_id + i).val(counter);
				calc_tot1 -= parseFloat(listprods[i].price) // calculation of total
			}
		}

		$("#tot_price").text(calc_tot1.toFixed(2));
	} else {
		$('#' + id).val(counter);
		// console.log("decrease");
		listprods[prodindexid].qty = counter;
		for (var i = 0; i < listprods.length; i++) {
			if (listprods[i].product_id == innerprdid) {
				$('#qty-val' + listprods[i].category_id + i).val(counter);
			}
		}
		// console.log(listprods);
	}

}

// increasing product count 
function prodcountplus(id, prodindexid, cat_name, flg) {
	// parameters(counterinputlabel,productindexid,counterinputlabel,flag)
	var counter = $('#' + id).val();
	counter++;

	// for details product view increment
	if (flg == 1) { // second div
		if ($('#prodQty_detail').length) {
			var innerprdid = $('#get_prod_inner').html();
			$('#' + id).val(counter);
			for (var i = 0; i < listprods.length; i++) {
				if (listprods[i].product_id == innerprdid) {
					$('#qty-val' + listprods[i].category_id + i).val(counter);
					listprods[i].qty = counter;
				}
			}
		}
	}
	else if (flg == 2) {     // fist div
		// console.log("Increase");
		listprods[prodindexid].qty = counter;
		// console.log(listprods);
		$(cat_name).val(counter);
	} else {

		$('#' + id).val(counter);
		var calc_tot2 = parseFloat($("#tot_price").text());
		for (var i = 0; i < listprods.length; i++) {
			var innerprdid = $('#' + cat_name).html();
			if (listprods[i].product_id == innerprdid) {
				listprods[i].qty = counter;
				$('#qty-val' + listprods[i].category_id + i).val(counter);
				calc_tot2 += parseFloat(listprods[i].price) // calculation of total
			}
		}

		$("#tot_price").text(calc_tot2.toFixed(2));

	}

}

// back icon functionality to list of products
$("#backicon_prods").click(function () {
	$("#backicon_prods").addClass("hide");
	$("#prods_dropdown").removeClass("hide");
	$("#prods_head").removeClass("hide");
	$(".small-nav").removeClass("hide");
	$("#items_list_attach").removeClass("hide");
	$("#items_detail_attach").addClass("hide");
	$("#products_checout").addClass("hide");
	$(".total-amount").addClass("hide");
	$(".new_date").addClass("hide");

})

// cart container view 
$("#cart-container").click(function () {

	if (($("#cart-cnt-val").html()) > 0) {
		$("#backicon_prods").removeClass("hide");
		$("#prods_dropdown").addClass("hide");
		$("#prods_head").addClass("hide");
		$(".small-nav").addClass("hide");
		$("#items_list_attach").addClass("hide");
		$("#items_detail_attach").addClass("hide");
		$("#products_checout").removeClass("hide");
		appdata.sel_product_cart();

	} else {
		var msg = "Your Shipping Cart is Empty"
		appdata.warningmsg(msg);
		return;
	}

	
})

// $("#products_pay").click(function () {
// 	appdata.productspayment();
// })
// products pay button 
// $("#products_pay").click(function () {
// 	appdata.productspayment();
// })

// global time functionality(controls delivery time for every product)
$("#globaltime").on("change", function () {
	appdata.productstimechange();
})

// global Date functionality(controls delivery date for every product)
// $("#globaldate").on("click", function () {
// 	appdata.productsdatechange();
// 	alert();
// })






// change password 
$("#btnChangepwd").on("click", function () {
	if ($("#txtvopwd").val() == "" || $("#txtvopwd").val() == undefined) {
		mes = "Please Enter Old Password";
		appdata.warningmsg(mes);
		return;
	}

	if ($("#txtvnpwd").val() == "" || $("#txtvnpwd").val() == undefined) {
		mes = "Please Enter New Password";
		appdata.warningmsg(mes);
		return;
	}

	if ($("#txtvcnfpwd").val() == "" || $("#txtvcnfpwd").val() == undefined) {
		mes = "Please Enter Confirm New Password";
		appdata.warningmsg(mes);
		return;
	}

	var changepwdbj = {
		email_id: localStorage.getItem("email"),
		old_password: $("#txtvopwd").val(),
		new_password: $("#txtvnpwd").val(),
		new_re_password: $("#txtvcnfpwd").val()
	};

	api.post1("changeuserpassword/", changepwdbj, function (res) {
		if (res == "Duplicate") {
			mes = "Old and New passwords should not be same";
			appdata.warningmsg(mes);
			return;
		} else {
			mes = "Change Password Successfully";
			appdata.successmsg(mes);
			$("#txtvopwd").val("");
			$("#txtvnpwd").val("");
			$("#txtvcnfpwd").val("");
		}
	});
});

